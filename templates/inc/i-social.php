<div class="social">
	<a href="#" title="Like Team Canada on Facebook" class="social-fb" rel="external">Like Team Canada on Facebook</a>
	<a href="#" title="Follow Team Canada on Twitter" class="social-tw" rel="external">Follow Team Canada on Twitter</a>
	<a href="#" title="Subscribe to Team Canada's YouTube Channel" class="social-yt" rel="external">Subscribe to Team Canada's YouTube Channel</a>
</div><!-- .social -->