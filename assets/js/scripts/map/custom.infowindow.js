;(function(context) {

	var CustomInfoWindow;
	
	var CustomInfoWindowClosure = function(opts,map) {

		CustomInfoWindow = function(opts,map) {
			this.map = map;
			this.visible = false;
			var div = '\
				<div class="custom-info-window-parent">\
					<div class="custom-info-window">\
						<div class="custom-info-window-content"></div>\
						<span class="pointer"></span>\
					</div>\
				</div>\
			';
			this.div = $(div);
			this.wrapperDiv = this.div.children('div.custom-info-window');
			this.contentDiv = this.wrapperDiv.children('div.custom-info-window-content');
			this.pointer = this.wrapperDiv.children('span.pointer');
			this.anchorOffset = {x:0,y:0};				
			map && this.setMap(map);
			
		};

		CustomInfoWindow.prototype = new google.maps.OverlayView();
		
		CustomInfoWindow.prototype.onAdd = function() {
			var stopPropagation = function(e) {
				var jqEvent;
			
				//stop propagation
				e.cancelBubble = true; 
				e.stopPropagation && e.stopPropagation(); 
			
				//send the event on up the chain via jQuery, but don't let it affect the map		
				jqEvent = new jQuery.Event(e.type,e);
				$(document).trigger(jqEvent);
			
				return false;
			}


			// We add an overlay to a map via one of the map's panes.
			// We'll add this overlay to the overlayImage pane.
			this.getPanes().floatPane.appendChild(this.div[0]);
			$(document).trigger('infoWindowAdded',[this]);
			
			 // catch mouse events and stop them propogating to the map
			  google.maps.event.addDomListener(this.div[0], 'DOMMouseScroll', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'click', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'contextmenu', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'dblclick', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'mousedown', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'mousemove', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'mouseup', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'mousewheel', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'touchend', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'touchmove', stopPropagation);
			  google.maps.event.addDomListener(this.div[0], 'touchstart', stopPropagation);
			  
			  return this.hide();
		};

		CustomInfoWindow.prototype.hide = function() { 
			var el = this.wrapperDiv;
			this.visible = false;
			el.removeClass('visible')
			this.div.trigger('infoWindowClosed');
			return this;
		};

		CustomInfoWindow.prototype.draw = function() {
			if(!this.visible) { return; }
			var
				projection = this.getProjection()
				anchorp = projection.fromLatLngToDivPixel(this.anchor) || {x:0,y:0};
				this.wrapperDiv.css('top',anchorp.y-this.offset.y).css('left',(anchorp.x-this.offset.x));
		};

		CustomInfoWindow.prototype.setContent = function(html) { 
			this.contentDiv.html(html);
			
			//force a redraw
			if(this.visible) {
				this.open(this.anchor);
			}
			
			return this;
		};

		CustomInfoWindow.prototype.open= function(anchor,opts) {
			var 
				width,height,centerVertically,
				tempPos = {},
				self = this,
				projection = this.getProjection(),
				bounds = this.map.getBounds(),
				ne = bounds.getNorthEast(),
				sw = bounds.getSouthWest(),
				
				//get the anchor position in pixels
				anchorp = projection.fromLatLngToDivPixel(anchor || this.opts.anchor),
				
				//get the bounds, in pixel values
				nep = projection.fromLatLngToDivPixel(ne),
				swp = projection.fromLatLngToDivPixel(sw),
				
				//pointer stuff - above | below | right | left
				pointerClass = false;
				
				width = this.div.children().outerWidth(),
				height = this.div.children().outerHeight();
			
			
				//set the anchor
				if(anchor) {
					this.anchor = anchor;
				}
				
				//position it in the center of the anchor horizontally
				tempPos.x = anchorp.x-(width/2);
				
				//position it above the anchor vertically
				tempPos.y = anchorp.y-height-this.anchorOffset.y;
				pointerClass = 'above';
				this.pointer
					.removeAttr('style')
					.removeClass()
					.addClass('pointer');
					
				
				//if the position is off the left (west) edge, place it all the way to the left edge of the map first and try again
				if(tempPos.x <= swp.x) { 
					tempPos.x = swp.x+10;
					
					//if this works and the info window is to the left of the anchor by more than 20px, then we're good
					if(tempPos.x <= (anchorp.x-20)) {
						console.log('here');
						this.pointer.css('left',anchorp.x-tempPos.x-(this.anchorOffset.x*2)-15);
						this.wrapperDiv
							.css('webkitTransformOriginX',anchorp.x-tempPos.x)
							.css('mozTransformOriginX',anchorp.x-tempPos.x)
							.css('msTransformOriginX',anchorp.x-tempPos.x)
							.css('oTransformOriginX',anchorp.x-tempPos.x)
							.css('transformOriginX',anchorp.x-tempPos.x);
					} else {
						//otherwise, place it to the right of the marker and center vertically
						tempPos.x = anchorp.x+(this.anchorOffset.x*2.5);
						centerVertically = true;
						pointerClass = 'right';
					}
				
				//otherwise, if the position is off the right (east) edge, place it all the way to the right edge of the map first and try again
				} else if(tempPos.x+width >= nep.x) {
					tempPos.x = nep.x-width-10;
					//if this works and the info window is to the right of the anchor by at least 20px, then we're good
					if(tempPos.x+width >= anchorp.x+20) {
						this.pointer.css('left',anchorp.x-tempPos.x-(this.anchorOffset.x*2)-15);
						this.wrapperDiv
							.css('webkitTransformOriginX',anchorp.x-tempPos.x)
							.css('mozTransformOriginX',anchorp.x-tempPos.x)
							.css('msTransformOriginX',anchorp.x-tempPos.x)
							.css('oTransformOriginX',anchorp.x-tempPos.x)
							.css('transformOriginX',anchorp.x-tempPos.x);				
					} else {
						//otherwise, place it to the left of the marker and center vertically
						tempPos.x = anchorp.x-width-(this.anchorOffset.x*2.5);
						centerVertically = true;
						pointerClass = 'left';
					}
					
				} else {
					//then we must be centered
					this.pointer.css('left','50%').css('marginLeft',-15);
				}
				
				if(centerVertically) {  
					tempPos.y = anchorp.y-(height/2)-(this.anchorOffset.y/2);  
				}
				
				//if the position is off the top edge, place it below the marker
				if(tempPos.y-40 <= nep.y) {
					tempPos.y = anchorp.y+15;
					
					//if the position was off the left or right edges to begin with, then position it along side the anchor
					if(centerVertically) {
						tempPos.y = nep.y+5;
						this.div.find('span.pointer').css('top',anchorp.y-tempPos.y);
					} else {
						pointerClass = 'below';
					}
					
				//if the position is off the bottom edge, place it at the bottom of the window
				} else if((tempPos.y+height) >= swp.y) {
					tempPos.y = swp.y-height-5;
					
					//if the position was off the left or right edges to begin with, then position it at the bottom of the window
					if(centerVertically) { 
						this.pointer.css('top',anchorp.y-tempPos.y); 
					}
				}
				
				this.wrapperDiv
					.removeClass('above below right left')
					.addClass(pointerClass)
					.addClass('visible')
					.css('top',tempPos.y)
					.css('left',tempPos.x); 
				
				this.offset = new google.maps.Point(anchorp.x-tempPos.x,anchorp.y-tempPos.y);
				this.visible = true;
			
			return this;
		};

		return new CustomInfoWindow(opts,map);
	};
	
	
	(function() {
		var cb = function(opts,map) {
			return CustomInfoWindow ? new CustomInfoWindow(opts,map) : CustomInfoWindowClosure(opts,map);
		}
		
		//CommonJS
		if(typeof module !== 'undefined' && module.exports) {
			module.exports = cb;
		//CodeKit
		} else if(context) {
			context.CustomInfoWindow = cb;
		}	
		
	}());
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));